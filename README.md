# DOUG Community Supported Resources website

The website for the DOUG Community Supported Resources project is managed by [Hugo](https://gohugo.io/), and hosted with Gitlab Pages.

## Contributing

The site itself can be maintained via merge request, without the need for Hugo.  Our GitlabCI pipeline does the actual Hugo build of static resources.

Fork this repository to your own branch and make your changes, then open a merge request to have them incorporated to the community site.

## Local Development

Developing locally it's useful to have Hugo installed, and running the built-in webserver makes checking your changes easy.

After installing the Hugo binary using whatever format is best for your platform, and from within the top-level of this repo (alongside the `config.toml` file), run:

```
# Starts the Hugo webserver
$ hugo -w server
```

Running the Hugo binary without flags will generate the static content for the site into a `public/` directory.  This is what the GitlabCI pipeline does, so the `public/` directory is excluded in the `.gitignore` file in this repo.

